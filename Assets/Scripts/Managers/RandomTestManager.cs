﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections;

public class RandomTestManager : MonoBehaviour
{
    public GameObject humanParent;
    public GameObject human;

    void GenerateHumans()
    {
        Transform[] items = humanParent.GetComponentsInChildren<Transform>();
        
        // Skip the first item because it is the parent object.
        for (int i = 1; i < items.Length; i++)
        {
            GameObject child = Instantiate(human, items[i], false) as GameObject;
            Destroy(child.GetComponent<DragScript>());
            child.GetComponent<Human>().Constructor();
        }
    }

	// Use this for initialization
	void Start ()
    {
        GenerateHumans();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
            return;
        }
        if (Input.GetButtonDown("ResetHumans"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(3);
        }
    }
}
