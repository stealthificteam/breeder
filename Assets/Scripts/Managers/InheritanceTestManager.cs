﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class InheritanceTestManager : MonoBehaviour
{
    public Transform greatGreatGrandparentsHolder;
    public Transform      greatGrandparentsHolder;
    public Transform           grandparentsHolder;
    public Transform                parentsHolder;

    public Transform kidHolder;

    public GameObject human;

    void GenerateHumans()
    {
        Transform[] greatGreatGrandparentsHolders = GetTransformChildList(greatGreatGrandparentsHolder);
        Transform[] greatGrandparentsHolders = GetTransformChildList(greatGrandparentsHolder);
        Transform[] grandparentsHolders = GetTransformChildList(grandparentsHolder);
        Transform[] parentsHolders = GetTransformChildList(parentsHolder);

        GameObject[] greatGreatGrandparents = GetFirstGeneration(greatGreatGrandparentsHolders);

        GameObject[] greatGrandparents = InstantiateGeneration(greatGrandparentsHolders, greatGreatGrandparents);

        GameObject[] grandparents =InstantiateGeneration(grandparentsHolders, greatGrandparents);

        GameObject[] parents = InstantiateGeneration(parentsHolders, grandparents);

        InstantiateGeneration(new Transform[1] { kidHolder }, parents);
    }

    GameObject[] GetFirstGeneration(Transform[] generation)
    {
        List<GameObject> instantiatedObjects = new List<GameObject>(generation.Length);

        foreach (Transform item in generation)
        {
            GameObject humanInstantiated = Instantiate(human, item.position, Quaternion.identity, item) as GameObject;
            humanInstantiated.GetComponent<Human>().Constructor();
            Destroy(humanInstantiated.GetComponent<DragScript>());

            instantiatedObjects.Add(humanInstantiated);
        }

        return instantiatedObjects.ToArray();
    }

    GameObject[] InstantiateGeneration(Transform[] generation, GameObject[] parentGeneration)
    {
        List<GameObject> instantiatedObjects = new List<GameObject>(generation.Length);

        // Starts at one to skip the holder object.
        for (int i = 0; i < generation.Length; i++)
        {
            GameObject humanInstantiated = Instantiate(human, generation[i].transform.position, Quaternion.identity, generation[i]) as GameObject;

            humanInstantiated.GetComponent<Human>().Constructor(parentGeneration[i * 2].GetComponent<Human>(), parentGeneration[i * 2 + 1].GetComponent<Human>());

            Destroy(humanInstantiated.GetComponent<DragScript>());

            instantiatedObjects.Add(humanInstantiated);

            DrawLine(generation[i], parentGeneration[i * 2].transform, parentGeneration[i * 2 + 1].transform);
        }

        return instantiatedObjects.ToArray();
    }

    Transform[] GetTransformChildList(Transform parent)
    {
        List<Transform> children = new List<Transform>();

        foreach (Transform item in parent)
        {
            children.Add(item);
        }

        return children.ToArray();
    }

    public Material lineMaterial;

    void DrawLine(Transform child, Transform mother, Transform father)
    {
        float width = 0.1f;

        LineRenderer motherLine = mother.gameObject.AddComponent<LineRenderer>();
        motherLine.SetPosition(0, mother.transform.position);
        motherLine.SetPosition(1, child.transform.position);
        motherLine.SetWidth(width, width);
        motherLine.material = lineMaterial;

        LineRenderer fatherLine = father.gameObject.AddComponent<LineRenderer>();
        fatherLine.SetPosition(0, father.transform.position);
        fatherLine.SetPosition(1, child.transform.position);
        fatherLine.SetWidth(width, width);
        fatherLine.material = lineMaterial;
    }

    void Start()
    {
        GenerateHumans();
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
            return;
        }
        if (Input.GetButtonDown("ResetHumans"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(2);
        }

    }
}
