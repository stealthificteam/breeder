﻿// Copyright 2016 Mick Zijdel under the MIT License.

public class HeightGene : FloatGene
{
    public const string geneName            = "HeightGene";

    const float geneMin                     = 1.4f;
    const float geneMax                     = 2.5f;

    const float geneMinStart                = 1.55f;
    const float geneMaxStart                = 1.8f;

    const float additionMin                 = 0.01f;
    const float additionMax                 = 0.1f;

    const float additionMinStart            = 0.01f;
    const float additionMaxStart            = 0.05f;

    // Growth is a what a Human will carry with him and won't be modified, only averaged to give it to children.
    const float growthMinStart              = 0.005f;
    const float growthMaxStart                = 0.01f;

    public HeightGene()
    {
        geneAdditionGrowth  = GetRandomFloat(growthMinStart      , growthMaxStart      );
        geneAddition        = GetRandomFloat(additionMinStart    , additionMaxStart  );
        gene                = GetRandomFloat(geneMinStart        , geneMaxStart      );

        SetStrings(geneName);
    }

    public HeightGene(HeightGene mother, HeightGene father)
    {
        geneAdditionGrowth = GetFloatFromParents(father.geneAdditionGrowth, mother.geneAdditionGrowth  , growthMinStart , growthMaxStart, 0);
        geneAddition       = GetFloatFromParents(father.geneAddition      , mother.geneAddition        , additionMin    , additionMax   , geneAdditionGrowth);
        gene               = GetFloatFromParents(father.gene              , mother.gene                , geneMin        , geneMax       , geneAddition);

        SetStrings(geneName);
    }
}
