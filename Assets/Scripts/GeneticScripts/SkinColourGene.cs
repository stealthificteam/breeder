﻿using UnityEngine;
using System.Collections;

public class SkinColourGene : FloatGene
{
    public const string geneName = "SkinColourGene";

    const float geneMin             = 0;
    const float geneMax             = 1f;

    const float geneMinStart        = 0;
    const float geneMaxStart        = 1f;

    //No need for addition and growth as SkinColour has bounds and won't change over generations when parents have the same genes.

    public SkinColourGene()
    {
        geneAdditionGrowth  = GetRandomFloat(0, 0);
        geneAddition        = GetRandomFloat(0, 0);
        gene                = GetRandomFloat(geneMinStart, geneMaxStart);

        SetStrings(geneName);
}

public SkinColourGene(SkinColourGene mother, SkinColourGene father)
    {
        geneAdditionGrowth  = GetRandomFloat(0, 0);
        geneAddition        = GetRandomFloat(0, 0);
        gene                = GetFloatFromParents(father.gene , mother.gene, geneMin, geneMax, 0);

        SetStrings(geneName);
    }
}
