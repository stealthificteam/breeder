﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System;

public class FloatGene
{
    public float gene                       { get; protected set; }
    public float geneAddition               { get; protected set; }
    public float geneAdditionGrowth         { get; protected set; }

    public string geneString                { get; protected set; }
    public string geneAdditionString        { get; protected set; }
    public string geneAdditionGrowthString  { get; protected set; }

    public string information               { get; protected set; }

    protected void SetStrings(string geneName)
    {
        geneString                  = GetFloatString(gene);
        geneAdditionString          = GetFloatString(geneAddition);
        geneAdditionGrowthString    = GetFloatString(geneAdditionGrowth);

        information                 = GetInformation(geneName);
    }

    public string GetInformation(string geneName)
    {
        return string.Format("{0} = {{gene = {1}; geneAddition = {2}; geneAdditionGrowth = {3}; }};", geneName, geneString, geneAdditionString, geneAdditionGrowthString);
    }



    public static void GetMinMax(float a, float b, out float min, out float max)
    {
        if (a < b)
        {
            min = a;
            max = b;
        }
        else //When they are equal it doesn't matter in which order they get returned so it can be the same.
        {
            min = b;
            max = a;
        }
    }

    public static float GetFloatFromParents(float father, float mother, float min, float max, float addition)
    {
        float output = 0;

        float minFromParents = 0, maxFromParents = 0;
        GetMinMax(father, mother, out minFromParents, out maxFromParents);

        output = GetRandomFloat(minFromParents, maxFromParents) + addition;

        return Mathf.Clamp(output, min, max);
    }

    public static float GetRandomFloat(float min, float max, bool log = false)
    {
        double output = min;

        // Add a random float between 0 and 1 i times where i is the amount of times 1 fits in the difference between min and max.
        for (int i = 0; i < (int)(max - min); i++)
        {
            output += Manager.random.NextDouble();
        }

        //If there is a bit remaining. Add a random float between 0 and 1 after multiplying it by the rest.
        if (Math.Abs((max - min) % 1) > 0)
        {
            output += Manager.random.NextDouble() * (max - min);
        }

        if(log)
        {
            Debug.Log(string.Format("Min: {0}; Max: {1}; Output: {2}; Abs(max - min): {3}; Abs(max - min % 1) > 0: {4};", min, max, output, Math.Abs((max - min) % 1), Math.Abs((max - min) % 1) > 0));
        }

        return (float)output;
    }

    public static string GetFloatString(float gene)
    {
        return gene.ToString();
    }

    /* Properties they all should have
    public const string geneName            = ;

    const float geneMin                     = ;
    const float geneMax                     = ;

    const float geneMinStart                = ;
    const float geneMaxStart                = ;

    const float additionMin                 = ;
    const float additionMax                 = ;

    const float additionMinStart            = ;
    const float additionMaxStart            = ;

    // Growth is a what a Human will carry with him and won't be modified, only averaged to give it to children.
    const float growthMinStart              = ;
    const float growMaxStart                = ;

    public <ClassName>()
    {
        geneAdditionGrowth  = GetRandomFloat(growthMinStart      , growMaxStart      );
        geneAddition        = GetRandomFloat(additionMinStart    , additionMaxStart  );
        gene                = GetRandomFloat(geneMinStart        , geneMaxStart      );

        SetStrings(geneName);
    }

    public <ClassName>(<ClassName> mother, <ClassName> father)
    {
        geneAdditionGrowth = GetFloatFromParents(father.geneAdditionGrowth, mother.geneAdditionGrowth  , 0);
        geneAddition       = GetFloatFromParents(father.geneAddition      , mother.geneAddition        , geneAdditionGrowth);
        gene               = GetFloatFromParents(father.gene              , mother.gene                , geneAddition);

        SetStrings(geneName);
    }
    */
}
