﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class AlleleGene
{
    public char[] gene { get; protected set; }

    public string geneString { get; protected set; }

    public string information { get; protected set; }

    protected void SetStrings(string geneName)
    {
        geneString  = GetGeneString(gene);
        information = GetInformation(geneName);
    }

    public string GetInformation(string geneName)
    {
        return string.Format("{0} = {{gene = {1}}};", geneName, geneString);
    }

    public static char[] GetGeneFromParents(int geneLength, string[] impossibleGenes, char[] fatherGene, char[] motherGene)
    {
        List<char> output = new List<char>(geneLength);

        int amountOfMotherGenes, amountOfFatherGenes;

        if (geneLength % 2 == 0)
        {
            amountOfFatherGenes = geneLength / 2;
            amountOfMotherGenes = geneLength / 2;
        }
        else
        {
            // To avoid bias to the father or the mother, example: 5 / 2 = 2 & 2, random = 1, so mother = 3 and father = 2.
            amountOfMotherGenes = Mathf.FloorToInt(geneLength / 2f);
            amountOfFatherGenes = Mathf.FloorToInt(geneLength / 2f);

            if (Manager.random.Next(2) == 1)
            {
                amountOfMotherGenes++;
            }
            else
            {
                amountOfFatherGenes++;
            }
        }

        output.AddRange(GrabRandomAlleles(fatherGene.ToList(), amountOfFatherGenes));
        output.AddRange(GrabRandomAlleles(motherGene.ToList(), amountOfMotherGenes));

        if (impossibleGenes.Contains(GetGeneString(output.ToArray())))
        {
            return fatherGene;
        }
        else
        {
            string outputString = "";
            foreach (char item in output)
            {
                outputString = outputString + item;
            }

            return output.ToArray();
        }
    }

    public static char[] GetRandomGene(int geneLength, string[] impossibleGenes, char[] recessiveAlleles, char[] dominantAlleles)
    {
        List<char> genes = new List<char>(recessiveAlleles.Length + dominantAlleles.Length);

        genes.AddRange(dominantAlleles);
        genes.AddRange(recessiveAlleles);

        char[] output = new char[geneLength];

        for (int i = 0; i < geneLength; i++)
        {
            output[i] = genes[Manager.random.Next(0, genes.Count)];
        }

        if (impossibleGenes.Contains(GetGeneString(output)))
        {
            return GetRandomGene(geneLength, impossibleGenes, recessiveAlleles, dominantAlleles);
        }
        else
        {
            return output;
        }
    }

    protected static List<char> GrabRandomAlleles(List<char> list, int amountOfGenes)
    {
        list.Shuffle();
        List<char> output = new List<char>(amountOfGenes);

        for (int i = 0; i < amountOfGenes; i++)
        {
            output.Add(list[i]);
        }
        return output;
    }

    public static string GetGeneString(char[] gene)
    {
        string outputString = "";
        foreach (var item in gene)
        {
            outputString = outputString + item;
        }

        return outputString;
    }

    /* Properties they all should have.
    public const string geneName                    ;

    protected readonly int      geneLength          ;

    protected readonly char[]   recessiveAlleles    ;
    protected readonly char[]   dominantAlleles     ;

    protected readonly List<string> impossibleGenes ; 

    public <ClassName>()
    {
        gene = AlleleGeneFunctions.GetRandomGene(geneLength, impossibleGenes, recessiveAlleles, dominantAlleles);
        SetStrings();
    }

    public <ClassName>(Human mother, Human father)
    {
        gene = AlleleGeneFunctions.GetGeneFromParents(geneLength, impossibleGenes, father.<HumanClassName>.gene, father.<HumanClassName>.gene);
        SetStrings();
    }
    */
}