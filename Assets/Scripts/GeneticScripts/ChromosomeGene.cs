﻿// Copyright 2016 Mick Zijdel under the MIT License.

using System.Collections.Generic;

public class ChromosomeGene : AlleleGene
{
    public const string geneName                = "ChromosomeGene";

    private readonly int geneLength             = 2;

    private readonly char[] recessiveAlleles    = new char[] { };
    private readonly char[] dominantAlleles     = new char[] { 'X', 'Y' };

    private readonly string[] impossibleGenes = new string[] { "YY" };

    public ChromosomeGene()
    {
        gene = GetRandomGene(geneLength, impossibleGenes, recessiveAlleles, dominantAlleles);
        SetStrings(geneName);
    }

    public ChromosomeGene(ChromosomeGene mother, ChromosomeGene father)
    {
        gene = GetGeneFromParents(geneLength, impossibleGenes, father.gene, mother.gene);
        SetStrings(geneName);
    }
}
