﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using UnityEngine.UI;

public class Human : MonoBehaviour
{
    new Renderer renderer;
    public Image sexSymbol;

    public Sprite marsSymbol;
    public Sprite venusSymbol;

    public Gender gender                        { get; private set; }

    public ChromosomeGene   chromosomeGenes     { get; private set; }
    public HeightGene       heightGenes         { get; private set; }
    public SkinColourGene   skinColourGenes     { get; private set; }

    public string information;

    public GameObject body;

    public Canvas canvas;

    void Awake()
    {
        renderer = body.GetComponent<Renderer>();        
    }

    void DefaultConstructor()
    {
        information = GetInformation();

        SetHeight();
        SetGender();
        SetSkinColour();
    }

    public void Constructor()
    {
        heightGenes     = new HeightGene();
        skinColourGenes = new SkinColourGene();

        chromosomeGenes = new ChromosomeGene();

        DefaultConstructor();
    }

    public void Constructor(Human mother, Human father)
    {
        heightGenes     = new HeightGene(mother.heightGenes, father.heightGenes);
        skinColourGenes = new SkinColourGene(mother.skinColourGenes, father.skinColourGenes);

        chromosomeGenes = new ChromosomeGene(mother.chromosomeGenes, father.chromosomeGenes);
        DefaultConstructor();
    }

    public string GetInformation()
    {
        return string.Format("{0} {1} {2}", chromosomeGenes.information, heightGenes.information, skinColourGenes.information);
    }

    private void SetHeight()
    {
        body.transform.Translate(new Vector3(0, (heightGenes.gene - 1) / 2));

        body.transform.localScale   = new Vector3(transform.localScale.x, heightGenes.gene      , transform.localScale.z);
        canvas.transform.localScale = new Vector3(transform.localScale.x, 1 / heightGenes.gene  , transform.localScale.z);

        BoxCollider collider        = GetComponent<BoxCollider>();
        collider.center             = new Vector3(collider.center.x, collider.center.y + (heightGenes.gene - 1) / 2);
        collider.size               = new Vector3(collider.size.x, heightGenes.gene, collider.size.z);
    }

    private void SetGender()
    {
        if (chromosomeGenes.geneString      == "XX")
        {
            gender = Gender.Female;
            if (sexSymbol != null && venusSymbol != null)
            {
                sexSymbol.sprite = venusSymbol;
            }
        }
        else if (chromosomeGenes.geneString == "XY" || chromosomeGenes.geneString == "YX")
        {
            gender = Gender.Male;
            if (sexSymbol != null && marsSymbol != null)
            {
                sexSymbol.sprite = marsSymbol;
            }
        }
        else
        {
            Debug.LogError("SetGender failed, chromosomeGenes string: " + chromosomeGenes.geneString);
        }
    }

    private void SetSkinColour()
    {
        string propertyName = "_X";
        if (renderer.material.HasProperty(propertyName))
        {
            renderer.material.SetFloat(propertyName, skinColourGenes.gene);
        }
        else
        {
            Debug.Log("The property " + propertyName + " does not exist!");
        }
    }
}

public enum Gender
{
    Male,
    Female
}