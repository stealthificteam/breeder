﻿using UnityEngine;
using System.Collections;

public class OptionsSideScreenScript : MonoBehaviour
{
    public MainMenuScript mainMenuScript;

	public void OnVolumeChange(float volume)
    {
        mainMenuScript.menuSoundAudioSource.volume = volume;
    }
}
