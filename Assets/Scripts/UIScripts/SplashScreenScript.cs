﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreenScript : MonoBehaviour
{
    public float totalWaitTime = 2f;
    public float fadeTime = 0.5f;

    public Image overlayImage;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(totalWaitTime - fadeTime);
        StartCoroutine("FadeOutCanvas");
    }

    IEnumerator FadeOutCanvas()
    {
        overlayImage.color = new Color(overlayImage.color.r, overlayImage.color.g, overlayImage.color.b, overlayImage.color.a + Time.deltaTime / fadeTime);

        yield return new WaitForEndOfFrame();

        if (overlayImage.color.a >= 1)
        {
            LoadMainMenu();
        }
        else
        {
            StartCoroutine("FadeOutCanvas");
        }
    }

    void LoadMainMenu()
    {
        SceneManager.LoadScene(1);
    }
}
