﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogScript : MonoBehaviour
{
    public Text dialogText;

    public void Confirm()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void Cancel()
    {
        this.GetComponent<Canvas>().enabled = false;
    }
}
