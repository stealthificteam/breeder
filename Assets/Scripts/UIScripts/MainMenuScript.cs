﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class MainMenuScript : MonoBehaviour
{
    public Canvas       creditsCanvas;
    public Canvas       helpCanvas;
    public Canvas       optionsCanvas;

    public float        fadeTime = 0.1f;

    public Image        overlayImage;

    public AudioSource  menuSoundAudioSource;
    public AudioClip    buttonClickSound;

    AudioSource musicPlayer = null;
    const float musicPlayerVolume = 0.4f;

    public void VarietyButton()
    {
        PlaySound(buttonClickSound);
        StartCoroutine("FadeOutCanvas", 3);
    }

    public void InheritanceButton()
    {
        PlaySound(buttonClickSound);
        StartCoroutine("FadeOutCanvas", 2);
    }

    public void SelectorButton()
    {
        PlaySound(buttonClickSound);
        StartCoroutine("FadeOutCanvas", 4);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    void LoadScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    void LoadScene(int sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    IEnumerator FadeOutCanvas(int sceneToLoad)
    {
        overlayImage.color = new Color(overlayImage.color.r, overlayImage.color.g, overlayImage.color.b, overlayImage.color.a + Time.deltaTime / fadeTime);

        yield return new WaitForEndOfFrame();

        if (overlayImage.color.a >= 1)
        {
            LoadScene(sceneToLoad);
        }
        else
        {
            StartCoroutine("FadeOutCanvas", sceneToLoad);
        }
    }

    void PlaySound(AudioClip clipToPlay)
    {
        menuSoundAudioSource.clip = clipToPlay;
        menuSoundAudioSource.Play();
    }

    public void SwitchScreen(string screenToLoad)
    {
        switch (screenToLoad.ToUpper())
        {
            case "CREDITS"  :
                SetSideScreen(ref creditsCanvas);
                break;
            case "HELP"     :
                SetSideScreen(ref helpCanvas);
                break;
            case "OPTIONS":
                SetSideScreen(ref optionsCanvas);
                break;
            default:
                Debug.Log("The screen " + screenToLoad + " can not be loaded.");
                break;
        }
    }
    
    void SetSideScreen(ref Canvas sideScreenToEnable)
    {
        if(sideScreenToEnable.enabled)
        {
            sideScreenToEnable.enabled = false;
        }
        else
        {
            sideScreenToEnable.enabled = true;
        }
        DisableOtherSideScreens(ref sideScreenToEnable);
    }

    void DisableOtherSideScreens(ref Canvas canvasToLeaveAlive)
    {
        DisableSideScreen(ref canvasToLeaveAlive, ref optionsCanvas);
        DisableSideScreen(ref canvasToLeaveAlive, ref helpCanvas);
        DisableSideScreen(ref canvasToLeaveAlive, ref creditsCanvas);
    }
    
    void DisableSideScreen(ref Canvas canvasToLeaveAlive, ref Canvas canvasToDisable)
    {
        if (canvasToDisable != canvasToLeaveAlive && helpCanvas.enabled)
        {
            canvasToDisable.enabled = false;
        }
    }

    public void ChangeVolume(float volume)
    {
        if(musicPlayer != null)
        {
            musicPlayer.volume = volume * musicPlayerVolume;
        }
    }

    void Start()
    {
        SetSideScreen(ref helpCanvas);
            musicPlayer = GetMusicPlayer();
    }

    AudioSource GetMusicPlayer()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        foreach (AudioSource item in audioSources)
        {
            if (item.name == "AudioSourceHolder")
            {
                return item;
            }
        }
        return null;
    }
}
