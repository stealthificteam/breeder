﻿// This code is partially from Unity Answers and partially by me.

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]

public class DragScript : MonoBehaviour
{
    private Vector3     screenPoint;
    private Vector3     offset;

    private Vector3     startPoint;

    private HumanSlotScript justLeftSlot = null;
    private HumanSlotScript slot = null;

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Slot" && collision.GetComponent<HumanSlotScript>().IsEmpty(gameObject))
        { 
                slot = collision.gameObject.GetComponent<HumanSlotScript>();
        }
    }

    void OnTriggerExit()
    {
        justLeftSlot = slot;
        slot = null;
    }

    void OnMouseDown()
    {
        startPoint = transform.position;

        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        transform.position = curPosition;
    }

    void OnMouseUp()
    {
        if(slot == null)
        {
            transform.position = startPoint;
            slot = justLeftSlot;
            justLeftSlot.GetComponent<HumanSlotScript>().LockToSlot(gameObject);
        }
        else
        {
            slot.LockToSlot(gameObject);
        }
    }
}