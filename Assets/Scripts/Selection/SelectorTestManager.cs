﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectorTestManager : MonoBehaviour
{
    public Canvas dialog;
    public GameObject slotParent;
    public GameObject human;

    void GenerateHumans()
    {
        List<Gender> humanGenders = new List<Gender>() { Gender.Male, Gender.Female, Gender.Male, Gender.Female, Gender.Male, Gender.Female, Gender.Male, Gender.Female };
        humanGenders.Shuffle();

        HumanSlotScript[] slots = slotParent.GetComponentsInChildren<HumanSlotScript>();

        for (int i = 0; i < slots.Length; i++)
        {
            GenerateHuman(slots[i], humanGenders[i]);
        }
    }
    
    void GenerateHuman(HumanSlotScript slot, Gender gender)
    {
        GameObject result = Instantiate(human, new Vector2(slot.transform.position.x, slot.transform.position.y), Quaternion.identity) as GameObject;
        result.GetComponent<Human>().Constructor();

        if(result.GetComponent<Human>().gender != gender)
        {
            Destroy(result);
            GenerateHuman(slot, gender);
        }
    }

    void Start()
    {
        GenerateHumans();
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            dialog.GetComponent<Canvas>().enabled = true;
            return;
        }
    }
}
