﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Matcher : MonoBehaviour
{
    public HumanSlotScript maleSlot;
    public HumanSlotScript femaleSlot;

    public GameObject resultPoint;
    public GameObject human;

    public void Match()
    {
        if(maleSlot.humanInSlot.GetComponent<Human>().gender == Gender.Male && femaleSlot.humanInSlot.GetComponent<Human>().gender == Gender.Female && resultPoint.GetComponent<HumanSlotScript>().humanInSlot == null)
        {
            GameObject child = Instantiate(human, resultPoint.transform.position, Quaternion.identity) as GameObject;
            
            child.GetComponent<Human>().Constructor(femaleSlot.humanInSlot.GetComponent<Human>(), maleSlot.humanInSlot.GetComponent<Human>());
        }
    }
}
