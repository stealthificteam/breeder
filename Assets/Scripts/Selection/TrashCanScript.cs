﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections;

public class TrashCanScript : HumanSlotScript
{
    override public void LockToSlot(GameObject objectToLock)
    {
        Destroy(objectToLock);
    }
}
