﻿// Copyright 2016 Mick Zijdel under the MIT License.

using UnityEngine;
using System.Collections;

public class HumanSlotScript : MonoBehaviour
{
    public GameObject humanInSlot { get; protected set; }

    protected virtual void Start()
    {
        humanInSlot = null;
    }

    protected virtual void OnTriggerEnter(Collider collider)
    {
        if(humanInSlot == null)
        {
            humanInSlot = collider.gameObject;
        }
    }

    protected virtual void OnTriggerExit(Collider collider)
    {
        if(collider.gameObject == humanInSlot)
        {
            humanInSlot = null;
        }
    }

	public virtual void LockToSlot(GameObject objectToLock)
    {
        if(humanInSlot == null || humanInSlot == objectToLock)
        {
            humanInSlot = objectToLock;
            objectToLock.transform.position = transform.position;
        }
    }
    
    public bool IsEmpty(GameObject objectToCheck = null)
    {
        return humanInSlot == null || humanInSlot == objectToCheck;
    }
}
