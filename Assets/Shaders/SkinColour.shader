﻿Shader "Custom/SkinColour" 
{
	Properties
	{
		_X("X", Range(0,1)) = 1
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex	: POSITION;
				float2 uv		: TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex	: SV_POSITIOn;
				float2 uv		: TEXCOORD0;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}

			float _X;

			float4 frag(v2f i) : SV_TARGET
			{
				float x = (_X * 0.6 + 0.35) * 6 + 1.2;

				float red = (-0.3812 * (x * x) + 1.7686 * x + 14.215) * (-0.3812 * (x * x) + 1.7686 * x + 14.215);
				float green = (-0.3693 * (x * x) + 1.5192 * x + 13.534) * (-0.3693 * (x * x) + 1.5192 * x + 13.534);
				float blue = 1 / ((0.0036 * (x * x) - 0.0153 * x + 0.0865) * (0.0036 * (x * x) - 0.0153 * x + 0.0865));

				return float4(red / 255, green / 255, blue / 255, 1);
			}

			ENDCG
		}
	}
}
